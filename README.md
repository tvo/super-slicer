## Introduction
This is a project to do some cool stuff with the cnc.
At its core, this is a tool to generate gcode from a model (stl).

##TLDR:
super-slicer

#Algorithms
I have come across two types of slicing algorighms. The first one where it follows the features of a model. This one is good for mechanical manufacturing operations, where exact dimensioning across complex geometry is important. The other approach is where the machine sweeps in a pattern (grid or spiral) around the model and varies the Z height. That way is good for carving weird stuff, but if you have a diagonal wall, it will look kinda funky.

This project focues on the first algorithm, following the features, not a pattern.

#Why was this project created?
I did a a lot of googling and I couldn't find free cam software that could cut diagonal lines that looked good. I didn't like fusion 360 because they save all your stuff on their cloud, and when I tried it out it was pretty clunky. It also didn't work on linux, which was a pretty big show stopper for me. I tried out another cam program called PyCam. I did get that working, but it had minor problem with the gcode incompatibility with my cnc machine (a few gcode commands that xcarve didn't support). The dealbreaker for me with PyCam is that it had the pattern algorithm. I tried a few other programs, but I couldn't get them to work. 

# Reference links

## What's contained in this project

