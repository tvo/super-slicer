namespace cam.Helpers
{
    public static class NumberHelpers
    {
        public static bool WithinInclusive(this double point, double low, double high) => point >= low && point <= high;
        public static bool WithinInclusive(this float point, float low, float high) => point >= low && point <= high;
    }
}