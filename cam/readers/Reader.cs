using System;
using System.IO;
using System.Linq;

namespace cam.readers
{
    public class Reader
    {
        public string Path { get; }

        public Reader(string path)
        {
            this.Path = path;
        }

        public IReader GetReader()
        {
            if(!this.Path.EndsWith(".stl"))
                throw new NotImplementedException("Only supports stl file type currently.");
            var file = File.OpenRead(Path);
            var readBytes = new byte[5];
            if(file.Read(readBytes, 0, 5) == 5)
            {
                var firstText = System.Text.Encoding.ASCII.GetString(readBytes, 0, 5);
                if(firstText.Equals("solid"))
                    return new FacetAsciiReader(File.ReadAllLines(Path).ToList());
                return new FacetBinaryReader(file);
            }

            throw new FormatException("Could not read file.");
        }
    }
}