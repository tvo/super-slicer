using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using cam.models;

namespace cam.readers
{
    public class FacetAsciiReader : IReader
    {
        private readonly List<string> LineBuffer = new List<string>();

        private const string ThreeNumbersPattern = @"(?'a'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?) (?'b'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?) (?'c'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)";
        private static readonly RegexOptions Options = RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.ExplicitCapture;
        private static readonly Regex VertexRegex = new Regex($"vertex {ThreeNumbersPattern}", Options);
        private static readonly Regex FacetNormalRegex = new Regex($"vertex {ThreeNumbersPattern}", Options);
        private static readonly Regex StartsWithRegex = new Regex(@"^\s*(vertex|facet normal|outer loop|endfacet|endloop)", Options);

        private List<string> InputLines { get; }

        public FacetAsciiReader(List<string> inputLines)
        {
            InputLines = inputLines;
        }

        private Triangle ReadTriangleFromBuffer()
        {
            if (LineBuffer.Count < 4)
                return null;
            var facetNormal = VertexRegex.Matches(LineBuffer[0]);
            if (LineBuffer[1] != "outer loop")
                return null;

            var vertices = new List<Vertex>(3);
            foreach (var line in LineBuffer.Skip(2))
            {
                if (line == "endloop")
                    break;

                var match = VertexRegex.Match(line);
                if (!match.Success)
                    return null;
                var vMatches = match.Groups.Where<Group>(g => g.Name.Equals("a") || g.Name.Equals("b") || g.Name.Equals("c"))
                	.Select(g => g.Value)
                    .Where(v => !string.IsNullOrWhiteSpace(v))
                    .ToList();
                if (vMatches.Count() != 3)
                    return null;
                var x = float.Parse(vMatches[0]);
                var y = float.Parse(vMatches[1]);
                var z = float.Parse(vMatches[2]);
                vertices.Add(new Vertex(x, y, z));
            }

            if (vertices.Count < 3)
                return null;

            return new Triangle(vertices[0], vertices[1], vertices[2]);
        }

        public List<Triangle> Read()
        {
            var triangles = new List<Triangle>();
            foreach (var line in InputLines)
            {
                if (!StartsWithRegex.IsMatch(line))
                    continue;

                LineBuffer.Add(line);
                if (line == "endfacet")
                {
                    var triangle = ReadTriangleFromBuffer();
                    if (triangle != null)
                        triangles.Add(triangle);
                    LineBuffer.Clear();
                }
            }
            return triangles;
        }

        public Model GetModel() => new Model(Read());
    }
}
