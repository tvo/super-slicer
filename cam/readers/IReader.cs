using cam.models;

namespace cam.readers
{
    public interface IReader
    {
        Model GetModel();
    }
}