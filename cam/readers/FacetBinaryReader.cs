using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using cam.models;

namespace cam.readers
{
    public class FacetBinaryReader : IReader, IDisposable
    {
        public FileStream InputFile { get; }

        public FacetBinaryReader(FileStream stream)
        {
            InputFile = stream;
        }

        public List<Triangle> Read(Stream stream)
        {
            var result = new List<Triangle>();
            // https://en.wikipedia.org/wiki/STL_(file_format)#Binary_STL
            stream.Position = 0;
            using (var sr = new BinaryReader(stream))
            {
                var header = new char[80];
                sr.Read(header, 0, 80); // discards

                var triangleCount = sr.ReadInt32();
                for (var i = 0; i < triangleCount; ++i)
                {
                    // normal vector. This is discarded.
                    float Read()
                    {
                        return (float)sr.ReadSingle();
                    }
                    Read(); Read(); Read(); // discards the "normal" vector.
                    var one =       new Vertex(Read(), Read(), Read());
                    var two =       new Vertex(Read(), Read(), Read());
                    var three =     new Vertex(Read(), Read(), Read());
                    var attribute = sr.ReadInt16();
                    result.Add(new Triangle(one, two, three));
                }
            }
            return result;
        }

        public void Dispose()
        {
            InputFile.Close();
            InputFile.Dispose();
        }

        public Model GetModel()
        {
            return new Model(Read(InputFile));
        }
    }
}