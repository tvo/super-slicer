using System.Collections.Generic;
using System.Linq;
using cam.models;

namespace cam.PathGenerators
{
	public class Basic
	{
		public Model model;

		public Basic(Model model)
		{
			this.model = model;
		}

		public List<List<PolyLine>> SliceModel(float stepHeight, Model model = null)
		{
			if(model is null)
				model = this.model;

			// get the max and min z point of the model.
			var allPoints = model.Triangles.SelectMany(t => new[] { t.One, t.Two, t.Three }).Select(p => p.Z).Distinct().ToList();
			var maxZ = allPoints.Max();
			var minZ = allPoints.Min();
			allPoints.Clear(); // don't need anymore.

			// slice the model via several planes parallel to the z plane.
			var slices = new List<List<PolyLine>>();
			for (var z = maxZ - stepHeight; z >= minZ; z -= stepHeight)
			{
				var ZPoint = Vertex.Zero + (z * Vector.UnitZ);
				var slicePlane = new Plane(ZPoint + Vector.UnitX, ZPoint + Vector.UnitY, ZPoint);

				var cutSegment = model.Triangles
					.Select(t => slicePlane.IntersectionOf(t))
					.ToList()
					.SelectMany(x => x)
					.Where(s => s != null).ToList();

				RemoveOverlappingLines(cutSegment);

				var lines = SortPolyLines(cutSegment);

				slices.Add(lines);
			}

			return slices.Where(x => x.Any()).ToList();
		}

		public List<PolyLine> ExpandToolPath(IEnumerable<PolyLine> slice, float toolDiameter)
		{
			toolDiameter = System.Math.Abs(toolDiameter);
			var result = new List<PolyLine>();
			foreach (var line in slice)
				result.Add(line.Expand(toolDiameter));
			return result;
		}

		public PolyLine AddInitialPlungeCut(PolyLine cutLine, float plungeDepth)
		{
			plungeDepth = System.Math.Abs(plungeDepth); // defensive programming :]
			var toPlungeTo = cutLine.Segments.First().One;
			var segments = new List<Segment>(cutLine.Segments.Count + 1);
			segments.Add(new Segment(toPlungeTo + Vector.UnitZ * plungeDepth, toPlungeTo));
			segments.AddRange(cutLine.Segments);
			return new PolyLine(segments);
		}

		public List<List<PolyLine>> TranslateHorizontalToVertical(List<List<PolyLine>> slices)
		{
			// TODO: there might be a hole or something where we need to slice down different paths in sections.
			return slices;
		}

		public PolyLine ConnectVerticalFeatures(List<PolyLine> verticalFeatureCutPaths)
		{
			// TODO: this only connects the last of one cutpath to the first of the next layer. Could cause the tool to move through the model.
			//return PolyLine.FromPoints(new PolyLine(verticalFeatureCutPaths.SelectMany(x => x.Segments)));
			return PolyLine.FromPoints(verticalFeatureCutPaths.SelectMany(p => p.ToVertices()).ToList());
		}

		public PolyLine ConnectSlices(List<PolyLine> layeredPaths)
		{
			// TODO: this only connects the last of one cutpath to the first of the next layer. Could cause the tool to move through the model.
			var vertices = new List<Vertex>(layeredPaths.Sum(x => x.Segments.Count * 2));
			if (layeredPaths is null || layeredPaths.Count <= 1)
				return layeredPaths?.FirstOrDefault();
			vertices.AddRange(layeredPaths.First().ToVertices());

			return PolyLine.FromPoints(vertices);
		}

		public List<List<PolyLine>> RemoveBridges(IEnumerable<List<PolyLine>> slices)
		{
			// TODO: make this work with multiplkihhe features.
			slices = slices.Where(x => x.Count >= 1);
			var cutSlices = slices.Select(x => x.First());
			var abovePath = cutSlices.First();

			var result = new List<List<PolyLine>>();
			result.Add(new [] { cutSlices.First() }.ToList());
			foreach(var slice in cutSlices.Skip(1)) // TODO: make sure that this goes from the top of the part to the bottom.
			{
				abovePath = abovePath.Union(slice, slice.Segments.First().One.Z);
				result.Add(new [] { abovePath }.ToList());
			}

			return result; // TODO: validate the order of returned slices also.
		}



		public List<List<PolyLine>> CutAboveLedges(List<List<PolyLine>> slices)
		{
			// TODO: implement
			return slices;
		}



		public List<Vertex> Process(float stepHeight, float toolDiameter)
		{
			var homed = model.HomeModel();
			var slices = SliceModel(stepHeight, homed);
			slices = RemoveBridges(slices);
			slices = CutAboveLedges(slices);
			var features = TranslateHorizontalToVertical(slices);
			var featureCutPaths = new List<PolyLine>();
			foreach(var feature in features)
			{
				var toolPathPerSlice = ExpandToolPath(feature, toolDiameter).ToList();
				var featureCutPath = ConnectSlices(toolPathPerSlice);
				featureCutPaths.Add(featureCutPath);
			}
			var cutPath = ConnectVerticalFeatures(featureCutPaths);
			cutPath = AddInitialPlungeCut(cutPath, stepHeight);
			return cutPath.ToVertices().ToList();
		}

		public void RemoveOverlappingLines(IList<Segment> segments)
		{
			var toRemove = new HashSet<int>();
			for (int i = 0; i < segments.Count; ++i)
			{
				var s1 = segments[i];
				for (var j = i + 1; j < segments.Count; ++j)
				{
					var s2 = segments[j];

					// Segments that are the same, remove one of them.
					if (s1.One.Equals(s2.One) && s1.Two.Equals(s2.Two))
					{
						toRemove.Add(j);
					}
					// Segments that are opposing.
					if (s1.One.Equals(s2.Two) && s1.Two.Equals(s2.One))
					{
						toRemove.Add(i);
						toRemove.Add(j);
					}
				}
			}
			var removeList = toRemove.OrderByDescending(x => x).ToList();
			foreach (var removeI in removeList)
			{
				segments.RemoveAt(removeI);
			}
		}


		public bool CanAddSegment(IList<Segment> segments, Segment segment)
		{
			return segments.Last().Two.CloseEnoughTo(segment.One);
		}

		public List<PolyLine> SortPolyLines(IEnumerable<Segment> segmentSource)
		{
			var lines = new List<PolyLine>();

			var segments = segmentSource.ToList();

			while (segments.Any())
			{
				var line = new List<Segment>();

				Segment toAdd = segments.First();
				line.Add(toAdd);
				segments.Remove(toAdd);

				while ((toAdd = segments.FirstOrDefault(s => CanAddSegment(line, s))) != null)
				{
					line.Add(toAdd);
					segments.Remove(toAdd);
				}

				lines.Add(new PolyLine(line));
			}

			return lines;
		}
	}
}
