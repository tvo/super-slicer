using System.Collections.Generic;
using System.Linq;
using cam.models;
using cam.models.gcode;

namespace cam.PathGenerators
{
	public class GCodeGenerator
	{
		public UnitSystem.System Units { get; }
		
		public GCodeGenerator(UnitSystem.System units)
		{
			this.Units = units;
		}

		public List<IGLine> GenerateGCode(IReadOnlyCollection<Vertex> toolPath)
		{
			var code = new List<IGLine>(toolPath.Count + 10);
			code.Add(new UnitSystem(Units));

			foreach(var point in toolPath)
				code.Add(new MoveToPoint(PositioningSpeed.Linear, point));

			code.Add(EndProgram.Instance);

			return code;
		}
	}
}