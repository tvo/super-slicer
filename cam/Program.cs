﻿using System;
using System.Collections.Generic;
using System.Linq;
using cam.models;
using cam.readers;
using CommandLine;

namespace cam
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<CommandLineOptions>(args)
                .WithParsed<CommandLineOptions>(opt =>
                {
                    if (string.IsNullOrEmpty(opt.FilePath) && opt.stdin == false)
                        return;
                    if (!string.IsNullOrEmpty(opt.FilePath) && opt.stdin)
                        return;

					var reader = new Reader(opt.FilePath).GetReader();
					var model = reader.GetModel();
					if(reader is IDisposable trash)
						trash.Dispose();

                    // process cutting paths.
                    var process = new PathGenerators.Basic(model);
                    var path = process.Process(opt.stepHeight, opt.toolDiameter);

                    // path => gcode.
                    var gcodeGenerator = new PathGenerators.GCodeGenerator(models.gcode.UnitSystem.System.Metric);
                    var code = gcodeGenerator.GenerateGCode(path).Select(x => x.Line());

                    // output gcode
                    if (opt.stdout)
                    {
                        foreach (var line in code)
                            Console.WriteLine(line);
                    }
                    if (opt.gcodePath != null)
                    {
                        System.IO.File.WriteAllLines(opt.gcodePath, code);
                    }
                })
                .WithNotParsed((errors) =>
                {
                    Console.WriteLine("Error!");

                    foreach (var error in errors)
                        Console.WriteLine(errors);
                });
        }
    }
}
