using System;
using Immutability;

namespace cam.models
{
	[Immutable]
	public class Vertex
	{
		public Vertex(float x, float y, float z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public float X { get; }
		public float Y { get; }
		public float Z { get; }


		public Vertex Round(int digits = 5)
		{
			return new Vertex(
				(float)System.Math.Round(X, digits),
				(float)System.Math.Round(Y, digits),
				(float)System.Math.Round(Z, digits));
		}


		public override bool Equals(object obj)
		{
			if (obj is Vertex)
			{
				var other = obj as Vertex;
				return X == other.X && Y == other.Y && Z == other.Z;
			}
			return false;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(X, Y, Z);
		}

		public override string ToString() => $"({X}, {Y}, {Z})";

		public static readonly Vertex Zero = new Vertex(0, 0, 0);

		public bool CloseEnoughTo(Vertex two) => (this - two).Magnitude() < 0.0001f;

		public static Vertex operator *(Vertex p, Vector scalar) => new Vertex(p.X * scalar.X, p.Y * scalar.Y, p.Z * scalar.Z);
		public static Vertex operator *(Vector scalar, Vertex p) => new Vertex(p.X * scalar.X, p.Y * scalar.Y, p.Z * scalar.Z);
		public static Vertex operator -(Vertex a, Vector direction) => new Vertex(a.X - direction.X, a.Y - direction.Y, a.Z - direction.Z);
		public static Vector operator -(Vertex a, Vertex b) => new Vector(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
		public static Vertex operator +(Vertex a, Vector direction) => new Vertex(a.X + direction.X, a.Y + direction.Y, a.Z + direction.Z);
	}
}
