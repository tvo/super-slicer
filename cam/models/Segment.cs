using System;
using cam.Helpers;
using Immutability;

namespace cam.models
{
	[Immutable]
	public class Segment
	{
		public Segment(Vertex one, Vertex two)
		{
			One = one;
			Two = two;
		}

		public Vertex One { get; }
		public Vertex Two { get; }

		public Vector Normal()
		{
			var angle = 90 * System.Math.PI / 180; // radians
			var x = System.Math.Cos(angle) * (Two.X - One.X) - System.Math.Sin(angle) * (Two.Y - One.Y);
			var y = System.Math.Sin(angle) * (Two.X - One.X) + System.Math.Cos(angle) * (Two.Y - One.Y);
			return new Vector((float)x, (float)y, 0).UnitLength();
		}

		public Vector Direction() => Two - One;

		public Segment Translate(Vector offset)
		{
			return new Segment(One + offset, Two + offset);
		}

		/// <summary>
		/// This is with reference to a dimension. The other segment is assumed to the be the next connecting line segment to this.
		/// The reference dimension is which dimension is assumed to be Z dimension. Both line segments should be parallel to this reference dimension plane.
		/// </summary>
		/// <returns>true for convex, null for straight, false for concave.
		public bool? ConvexAngle(Segment other, Dimension referenceDimension = Dimension.Z)
		{
			var cross = Direction().CrossProduct(other.Direction());
			if (cross.Equals(Vector.Zero)) return null; // straight line.

			if (referenceDimension == Dimension.Z) return cross.Z < 0;
			if (referenceDimension == Dimension.Y) return cross.Y < 0;
			if (referenceDimension == Dimension.X) return cross.X < 0;

			return null; // ~unimplemented~ - possible other cases/dimensions go here.
		}

		public (Segment, Segment) ToIntersection(Segment other)
		{
			var ray = new Ray(One, Direction());
			var otherRay = new Ray(other.One, other.Direction());

			var intersectionPoint = ray.Intersects(otherRay);

			var left = new Segment(One, intersectionPoint);
			var right = new Segment(intersectionPoint, other.Two);
			return (left, right);
		}

		// Given three colinear points p, q, r, the function checks if
		// point q lies on line segment 'pr' 
		public Vertex IntersectionOf(Segment other)
		{
			// get directions sorted out.
			var dA = Direction();
			var dB = other.Direction();
			var dC = other.One - One;

			if (dC.DotProduct(dA.CrossProduct(dC)) != 0) // lines are not coplanar
				return null;

			var dA_Cross_Db = dA.CrossProduct(dB);
			var dA_Cross_Db_Magnitude = dA_Cross_Db.MagnitudeSquared();

            if(dA_Cross_Db_Magnitude == 0)
                return null;

			var s = dC.CrossProduct(dB).DotProduct(dA_Cross_Db) / dA_Cross_Db_Magnitude;
			var t = dC.CrossProduct(dA).DotProduct(dA_Cross_Db) / dA_Cross_Db_Magnitude;

			if (s.WithinInclusive(0, 1) && t.WithinInclusive(0, 1))
				return One + dA * s;
			return null;
		}

		public override bool Equals(object obj)
		{
			if (obj is Segment)
			{
				var seg = obj as Segment;
				return One.Equals(seg.One) && Two.Equals(seg.Two);
			}
			return false;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(One, Two);
		}

		public override string ToString() => $"{{{One} => {Two}}}";

		internal Segment Round(int digits = 5)
		{
			return new Segment(One.Round(digits), Two.Round(digits));
		}

		public static Segment operator +(Vector offset, Segment segment) => new Segment(segment.One + offset, segment.Two + offset);
		public static Segment operator +(Segment segment, Vector offset) => offset + segment;

		public static readonly Triangle IsoscelesRightTriangle = new Triangle(Vertex.Zero, Vertex.Zero + Vector.UnitY, Vertex.Zero + Vector.UnitX);
	}
}
