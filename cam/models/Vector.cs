using System;
using Immutability;

namespace cam.models
{
	[Immutable]
	public class Vector
	{
		public Vector(float x, float y, float z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public float X { get; }
		public float Y { get; }
		public float Z { get; }

        public float Magnitude() => (float)System.Math.Sqrt(MagnitudeSquared());
        public float MagnitudeSquared() => (X * X) + (Y * Y) + (Z * Z);

		public Vector Round()
		{
			return new Vector(
				(float)System.Math.Round(X, 5),
				(float)System.Math.Round(Y, 5),
				(float)System.Math.Round(Z, 5));
		}

		public static readonly Vector UnitX = new Vector(1, 0, 0);
		public static readonly Vector UnitY = new Vector(0, 1, 0);
		public static readonly Vector UnitZ = new Vector(0, 0, 1);
		public static readonly Vector Zero = new Vector(0, 0, 0);

		public Vector CrossProduct(Vector other)
		{
			var a = (Y * other.Z) - (Z * other.Y);
			var b = (Z * other.X) - (X * other.Z);
			var c = (X * other.Y) - (Y * other.X);

			return new Vector(a, b, c);
		}

		public float DotProduct(Vector other) => (X * other.X) + (Y * other.Y) + (Z * other.Z);

		public Vector UnitLength() => this / Magnitude();

		public Vector ProjectTo(Vector other)
		{
			other = other.UnitLength();
			var projection = other * (DotProduct(other));

			return projection;
		}


		public override bool Equals(object obj)
		{
			if (obj is Vector)
			{
				var other = obj as Vector;
				return X == other.X && Y == other.Y && Z == other.Z;
			}
			return false;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(X, Y, Z);
		}

		public override string ToString() => $"({X}, {Y}, {Z})";

		public static Vector operator *(float scalar, Vector vector) => new Vector(vector.X * scalar, vector.Y * scalar, vector.Z * scalar);
		public static Vector operator *(Vector scalar, Vector vector) => new Vector(vector.X * scalar.X, vector.Y * scalar.Y, vector.Z * scalar.Z);
		public static Vector operator *(Vector vector, float scalar) => scalar * vector;
		public static Vector operator /(Vector vector, float scalar) => new Vector(vector.X / scalar, vector.Y / scalar, vector.Z / scalar);

		public static Vector operator -(Vector vector, Vector other) => new Vector(vector.X - other.X, vector.Y - other.Y, vector.Z - other.Z);
		public static Vector operator +(Vector vector, Vector other) => new Vector(vector.X + other.X, vector.Y + other.Y, vector.Z + other.Z);
	}
}
