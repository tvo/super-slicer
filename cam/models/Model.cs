using System.Collections.Generic;
using System.Linq;
using Immutability;

namespace cam.models
{
    [Immutable]
    public class Model
    {
        public IReadOnlyCollection<Triangle> Triangles { get; }

        public Model(IEnumerable<Triangle> triangles)
        {
            Triangles = triangles.ToList().AsReadOnly();
        }


		public Model HomeModel()
		{
			var allPoints = Triangles.SelectMany(t => new[] { t.One, t.Two, t.Three }).ToList();
			var home = new Vector(allPoints.Min(p => p.X), allPoints.Min(p => p.Y), allPoints.Max(p => p.Z));
			return Translate(home * -1);
		}

        public Model Translate(Vector translationVector) => new Model(Triangles.Select(t => t.Translate(translationVector)));

		public Model Scale(Vector scaleVector) => new Model(Triangles.Select(t => t.Scale(scaleVector)));

		public List<Vertex> GetVertices()
		{
			return Triangles.SelectMany(t => new [] { t.One, t.Two, t.Three }).ToList();
		}

		/// Combines all triangles with this model with the other model. Probably a dumb blond or something. Typical.
		public Model Union(Model otherModel)
		{
			return new Model(new []{ Triangles, otherModel.Triangles }.SelectMany(x => x));
		}

		public static Model Cube()
		{
			Triangle Tri(params float[] ps)
			{
				return new Triangle(
						new Vertex(ps[0], ps[1], ps[2]),
						new Vertex(ps[3], ps[4], ps[5]),
						new Vertex(ps[6], ps[7], ps[8])
				);
			}
			var triangles = new []{
				Tri(1, 1, 1,  -1, 1, 1,  -1, -1, 1),
				Tri(1, 1, 1,  -1, -1, 1,  1, -1, 1),
				Tri(1, -1, -1,  1, -1, 1,  -1, -1, 1),
				
				Tri(1, -1, -1,  -1, -1, 1,  -1, -1, -1),
				Tri(-1, -1, -1,  -1, -1, 1,  -1, 1, 1),
				Tri(-1, -1, -1,  -1, 1, 1,  -1, 1, -1),
				
				Tri(-1, 1, -1,  1, 1, -1,  1, -1, -1),
				Tri(-1, 1, -1,  1, -1, -1,  -1, -1, -1),
				Tri(1, 1, -1,  1, 1, 1,  1, -1, 1),

				Tri(1, 1, -1,  1, -1, 1,  1, -1, -1),
				Tri(-1, 1, -1,  -1, 1, 1,  1, 1, 1),
				Tri(-1, 1, -1,  1, 1, 1,  1, 1, -1)
			};
			return new Model(triangles);
		}
    }
}
