using System;
using System.Collections.Generic;
using System.Linq;
using Immutability;

namespace cam.models
{
    [Immutable]
    public class Plane
    {
        public Plane(Vertex one, Vertex two, Vertex three)
        {
            One = one;
            Two = two;
            Three = three;
        }

        public Plane(Triangle reference)
        {
            One = reference.One;
            Two = reference.Two;
            Three = reference.Three;
        }

        public Vertex One { get; }
        public Vertex Two { get; }
        public Vertex Three { get; }

        public Vector Normal()
        {
            return new Triangle(One, Two, Three).CalculateNormal().UnitLength();
        }

        public PlanarEquation ToPlaneEquation()
        {
            // TODO: simplify this after tests cover this functionallity.
            var a = (Two.Y - One.Y) * (Three.Z - One.Z) - (Three.Y - One.Y) * (Two.Z - One.Z);
            var b = (Two.Z - One.Z) * (Three.X - One.X) - (Three.Z - One.Z) * (Two.X - One.X);
            var c = (Two.X - One.X) * (Three.Y - One.Y) - (Three.X - One.X) * (Two.Y - One.Y);
            var d = -(a * One.X + b * One.Y + c * One.Z);
            return new PlanarEquation(a, b, c, d);
        }

        public bool? IsOnNormalSide(Vertex point)
        {
            // unitlength vector
            var normal = Normal();

            var x = point - One; // +vector from a known vertex on the plane to the vector "point"

            // project x onto the normal
            var projection = normal * (x.DotProduct(normal));
            if (projection.Equals(Vector.Zero))
                return null;
            projection = projection.UnitLength();

            // check if the difference between the normal and the unit length projection is 0.
            var distance = (normal - projection).Magnitude();

            return Math.Abs(distance) < 0.0001;
        }

        public List<Segment> IntersectionOf(Triangle triangle)
        {
            var oneNormal = IsOnNormalSide(triangle.One);
            var twoNormal = IsOnNormalSide(triangle.Two);
            var threeNormal = IsOnNormalSide(triangle.Three);

            // count all nulls for 
            var nullCount = 0;
            if(oneNormal is null) ++nullCount;
            if(twoNormal is null) ++nullCount;
            if(threeNormal is null) ++nullCount;

            if(nullCount == 1) // only a single point intersects with the plane, ignore this case.
                return new List<Segment>();

            if (nullCount == 3) // the triangle is in the same plane with this plane.
                // return triangle.GetAllSides();
                return new List<Segment>(); // we don't actually want to return anything

            // if(nullCount == 2) // One side is in the plane. If point below it is 
            // {
            //     if(oneNormal == false || twoNormal == false || threeNormal == false)
            // }

            // if all three are not on the plane, default the ones are are exactly on the plane to be on one side.
            oneNormal = oneNormal ?? true;
            twoNormal = twoNormal ?? true;
            threeNormal = threeNormal ?? true;

            // if only one triangle edge hits the plane, return an empty 

            if (oneNormal.Value && twoNormal.Value && threeNormal.Value)
                return new List<Segment>();
            if (!oneNormal.Value && !twoNormal.Value && !threeNormal.Value)
                return new List<Segment>();

            Segment side1, side2, side3;

            side1 = new Segment(triangle.One, triangle.Two);
            side2 = new Segment(triangle.One, triangle.Three);
            side3 = new Segment(triangle.Three, triangle.Two);

            var sides = new List<Segment>();

            // get the two sides that the 
            if (oneNormal != twoNormal)
                sides.Add(side1);
            if (oneNormal != threeNormal)
                sides.Add(side2);
            if (twoNormal != threeNormal)
                sides.Add(side3);

            return new List<Segment> { new Segment(IntersectionOf(sides[0]), IntersectionOf(sides[1])) };
        }

        public Vertex IntersectionOf(Segment segment)
        {
            var normal = Normal();
            var u = segment.Two - segment.One; // vector in the direction of the line
            var w = segment.One - One; //
            var t = (-1 * normal).DotProduct(w) / normal.DotProduct(u);
            return segment.One + (u * t);
        }
    }
}