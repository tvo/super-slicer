using System.Collections.Generic;
using System.Linq;
using Immutability;

namespace cam.models
{
	[Immutable]
	public class PolyLine
	{
		public IReadOnlyCollection<Segment> Segments { get; }

		public PolyLine(Triangle triangle) : this(triangle.GetAllSides()) { }
		public PolyLine(IEnumerable<Segment> segments)
		{
			Segments = segments.ToList().AsReadOnly();
		}

		public IEnumerable<Vertex> ToVertices()
		{
			var verticies = new List<Vertex>();
			if(Segments.Count == 0)
				return verticies;
			verticies.Add(Segments.First().One);
			foreach(var s in Segments)
				verticies.Add(s.Two);
			return verticies;
		}

		public PolyLine Translate(Vector offset)
		{
			return new PolyLine(Segments.Select(x => x.Translate(offset)));
		}
		
		private NetTopologySuite.Geometries.Geometry GetPolygon()
		{
			var reader = new NetTopologySuite.IO.WKTReader();
			var pointString = string.Join(",", ToVertices().Select(v => $"{v.X} {v.Y}"));
			return reader.Read($"POLYGON(({pointString}))");
		}

		public PolyLine Union(PolyLine other, float? zHeight = null)
		{
			// get points in the net topology terms.
			var me = GetPolygon();
			var otherPoly = other.GetPolygon();

			var family = me.Union(otherPoly);
			var firstSegment = Segments.FirstOrDefault();
			float z;
			if(zHeight == null)
				z = firstSegment?.One.Z ?? 0;
			else
				z = zHeight.Value;

			var unionPoints = family.Coordinates.Select(c => new Vertex((float)c.X, (float)c.Y, z)).ToList();
			
			return FromPoints(unionPoints);			
		}

		public static PolyLine FromPoints(IEnumerable<Vertex> vertices)
		{
			var cnt = vertices.Count();
			var ator = vertices.GetEnumerator();
			if(cnt <= 1)
				return null;
			if(cnt == 2)
				return new PolyLine(new []{ new Segment(vertices.First(), vertices.Last()) });
			var segs = new List<Segment>(cnt / 2);
			ator.MoveNext();
			var currentPoint = ator.Current;
			for(var i = 0; i < cnt - 1; ++i)
			{
				ator.MoveNext();
				var next = ator.Current;
				segs.Add(new Segment(currentPoint, next));
				currentPoint = next;
			}
			return new PolyLine(segs);
		}

		public (Segment, Vertex) IntersectionOfSegment(Segment segment)
		{
			foreach (var s in Segments)
			{
				var point = segment.IntersectionOf(s);
				if (point != null)
				{
					return (s, point);
				}
			}
			return (null, null);
		}

		public override string ToString()
		{
			return this.ToString(Segments);
		}

		public string ToString(IEnumerable<Segment> segs)
		{
			if(segs is null)
				segs = Segments;
			return string.Join(", ", segs.Select(s => s.ToString()));
		}

		public bool IsClockwise() => Segments.Sum(s => (s.Two.X - s.One.X) * (s.Two.Y + s.One.Y)) > 0f;

		public PolyLine Expand(float offset)
		{
			var expandList = new List<Segment>(Segments.Count);

			// shift each line via offset.
			foreach (var segment in Segments)
			{
				// create a new line in the normal direction (90 deg to the segment) of same length;
				var lineOffset = segment.Normal().Round() * offset;

				// return if the normal failed for divide by 0 error. Line of 0 magnitude?
				if(lineOffset.X == float.NaN || lineOffset.Y == float.NaN || lineOffset.Z == float.NaN)
					continue;

				var toInsert = segment + lineOffset;
				expandList.Add(toInsert);
			}

			if (expandList.Count <= 1 || expandList.Count != Segments.Count)
				return null;

			// iterate over each corner to modify the shape of the corner.
			var segmentI = 0;
			for (var i = 0; i < expandList.Count; ++i, ++segmentI)
			{
				var leftI = i % expandList.Count;
				var rightI = (i + 1) % expandList.Count;
				segmentI = segmentI % Segments.Count; // don't overflow.

				// find the left and right line.
				var left = expandList[leftI];
				var right = expandList[rightI];

				var convex = left.ConvexAngle(right);

				// straight line lines. Don't need to do anything.
				if (convex is null) // straight - simplify straight line.
				{
					var straightSegment = new Segment(left.One, right.Two);
					expandList[leftI] = straightSegment;
					expandList.RemoveAt(rightI);
					--i; // because we are removing an element, we need to interate over this index again.
				}
				else if (convex.Value) // convex
				{
					var (newLeft, cornerSegment, newRight) = GetContuationOfLinesAroundCorner(left, right, Segments.ElementAt(segmentI).Two, offset);

					expandList[leftI] = newLeft;
					expandList[rightI] = cornerSegment;
					if (rightI != expandList.Count)
						expandList.Insert(rightI + 1, newRight);
					else
						expandList.Add(newRight);
					++i; // since we added another line segment, we don't want to iterate over the corner we just added.
					//++segmentI;
				}
				else // concave
				{
					var (newLeft, newRight) = left.ToIntersection(right);
					expandList[leftI] = newLeft;
					expandList[rightI] = newRight;
				}
			}

			return new PolyLine(expandList);
		}

		public static (Segment, Segment, Segment) GetContuationOfLinesAroundCorner(Segment left, Segment right, Vertex corner, float offset)
		{
			// corner is the point at which the segments meet up.
			var leftDirection = left.Direction();

			// c is the offset vector in the direction the lines are pointed towards.
			var c = ((leftDirection).UnitLength() + (right.One - right.Two).UnitLength()).UnitLength() * offset;

			// extend
			var vLeft = left.Two - corner;
			var vCenter = vLeft.ProjectTo(c);
			var dirToLeft = (vLeft - vCenter).UnitLength();

			var cToLeftVector = new Ray(corner + c, dirToLeft);
			var leftRay = new Ray(left.Two, leftDirection);

			var leftCorner = leftRay.Intersects(cToLeftVector);
			var distFromLeft = corner - leftCorner + c;
			var rightCorner = corner + c + distFromLeft;

			// Note: it needs the points to be rounded, otherwise there is some strange behavior around floating point precision and NaN returned from this fun.
			var cornerSegment = new Segment(leftCorner.Round(), rightCorner.Round());

			var newLeft = new Segment(left.One, cornerSegment.One);
			var newRight = new Segment(cornerSegment.Two, right.Two);

			return (newLeft, cornerSegment, newRight);
		}

		public static PolyLine Square(float width = 1.0f)
		{
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;
			var s1 = new Segment(one, four);
			var s2 = new Segment(four, three);
			var s3 = new Segment(three, two);
			var s4 = new Segment(two, one);
			return new PolyLine(new[] { s1, s2, s3, s4 });
		}
	}
}
