using System.Collections.Generic;
using Immutability;

namespace cam.models
{
    [Immutable]
    public class Triangle
    {
        public Triangle(Vertex one, Vertex two, Vertex three)
        {
            One = one;
            Two = two;
            Three = three;
        }

        public Vertex One { get; }
        public Vertex Two { get; }
        public Vertex Three { get; }

        public Vector CalculateNormal()
        {
            var u = Two - One;
            var v = Three - One;
            return u.CrossProduct(v);
        }

        public Triangle Translate(Vector translationVector)
        {
            return new Triangle(One + translationVector, Two + translationVector, Three + translationVector);
        }

		public Triangle Scale(Vector scaleVector)
		{
			return new Triangle(One * scaleVector, Two * scaleVector, Three * scaleVector); 
		}

        public List<Segment> GetAllSides()
        {
            return new List<Segment>() {
                new Segment(One, Two),
                new Segment(Two, Three),
                new Segment(Three, One)
            };
        }
    }
}
