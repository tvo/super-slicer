using System;
using Immutability;

namespace cam.models
{
    public enum Dimension
    {
        X, Y, Z
    }
}