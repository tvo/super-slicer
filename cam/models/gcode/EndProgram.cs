namespace cam.models.gcode
{
    public sealed class EndProgram : IGLine
    {
        private EndProgram()
        {
        }

        public static readonly EndProgram Instance = new EndProgram();

        public string Line() => "M02";
    }
}