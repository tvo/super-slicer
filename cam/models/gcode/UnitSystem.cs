namespace cam.models.gcode
{
    public class UnitSystem : IGLine
    {
        public enum System
        {
            Metric,
            Imperial,
        }

        public System system { get; }

        public UnitSystem(System system)
        {
            this.system = system;
        }

        public string Line()
        {
            switch(system)
            {
                default:
                case System.Metric:
                    return "G21";
                case System.Imperial:
                    return "G20";
            }
            
        }
    }
}