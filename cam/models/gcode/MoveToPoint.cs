namespace cam.models.gcode
{
    public enum MoveType
    {
        Relative,
        Absolute
    }

    public enum PositioningSpeed
    {
        Rapid,
        Linear
    }

    public class MoveToPoint : IGLine
    {
        public PositioningSpeed Speed { get; }
        public Vertex Point { get; }
        public MoveToPoint(PositioningSpeed speed, Vertex point)
        {
            this.Point = point;
            this.Speed = speed;
        }

        public string Line()
        {
            var speedCode = "";
            if(Speed == PositioningSpeed.Linear)
                speedCode = "G01";
            else if(Speed == PositioningSpeed.Rapid)
                speedCode = "G00";
            else throw new System.NotImplementedException();

            return $"{speedCode} X{Point.X:#0.0####} Y{Point.Y:#0.0####} Z{Point.Z:#0.0####}";
        }
    }
}