namespace cam.models.gcode
{
    // view gcode here: https://nraynaud.github.io/webgcode/
    public interface IGLine
    {
         string Line();
    }
}