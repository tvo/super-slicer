using System;
using Immutability;

namespace cam.models
{
    [Immutable]
    public class PlanarEquation
    {
        public PlanarEquation(float a, float b, float c, float d)
        {
            A = a;
            B = b;
            C = c;
            D = d;
        }

        /// X intercept
        public float A { get; }

        /// Y intercept
        public float B { get; }

        /// Z Intercept
        public float C { get; }

        /// Offset
        public float D { get; }

        public float CalculateDistanceToPoint(Vertex point)
        {
            var denominator = (A * A) + (B * B) + (C * C);
            if (denominator == 0)
                throw new NotImplementedException();
            denominator = (float)Math.Sqrt(denominator);

            var numerator = Math.Abs((A * point.X) + (B * point.Y) + (C * point.Z) + D);
            var magnitude = numerator / denominator;

            return magnitude;
        }
    }
}