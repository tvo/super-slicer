using Immutability;

namespace cam.models
{
    [Immutable]
    public class Ray
    {
        public Vertex Origin { get; }
        public Vector Direction { get; }

        public Ray(Vertex origin, Vector direction)
        {
            Origin = origin;
            Direction = direction;
        }

        public Vertex Intersects(Ray other)
        {
            var g = other.Origin - Origin;
            var h = other.Direction.CrossProduct(g).Magnitude();
            var k = other.Direction.CrossProduct(Direction).Magnitude();

            // no solution check.
            if(h == 0 || k == 0)
                return null;

            var f = h/k*Direction;
            if((h > 0 && k > 0) || (h < 0 && k < 0))
                return Origin + f;
            else
                return Origin - f;
        }
    }
}