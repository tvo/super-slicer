using CommandLine;

namespace cam
{
    public class CommandLineOptions
    {
        [Option('f', "file", Required = false, HelpText = "File of the stl to read.")]
        public string FilePath { get; set; }

        [Option('i', "stdin", Default = false, HelpText = "Read from stdin")]
        public bool stdin { get; set; }

        [Option('a', "ascii", Default = false, HelpText = "stl ascii format")]
        public bool AsciiFormat { get; set; }

        [Option('t', "toolDiameter", Default = 1, HelpText = "The diameter of the tool.")]
        public float toolDiameter { get; set; }

        [Option('s', "stepHeight", Default = 1, HelpText = "Slices the model into multiple planes. This is the distance between each plane.")]
        public float stepHeight { get; set; }

        [Option('o', "stdout", Default = true, HelpText = "Outputs gcode to stdout. Default true.")]
        public bool stdout { get; set; }

        [Option('g', "gcodePath", Default = null, HelpText = "Output file for all gcode lines to be written.")]
        public string gcodePath { get; set; }
    }
}