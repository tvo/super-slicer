using System;
using cam.models;
using Xunit;
using FluentAssertions;

namespace test
{
    public class SegmentTests
    {
        [Fact]
        public void PropertiesSetCorrectly()
        {
            var segment = new Segment(Vertex.Zero, Vertex.Zero + Vector.UnitX);
            segment.One.Should().Be(Vertex.Zero);
            segment.Two.Should().Be(Vertex.Zero + Vector.UnitX);
        }

        [Fact]
        public void Normal_LengthOfOne()
        {
            var segment = new Segment(Vertex.Zero, Vertex.Zero + Vector.UnitX);
            var normal = segment.Normal();
            normal.Magnitude().Should().Be(1);
        }

        [Fact]
        public void Normal()
        {
            var segment = new Segment(Vertex.Zero, Vertex.Zero + Vector.UnitX);
            var normal = segment.Normal();
            normal = normal.Round();
            normal.X.Should().Be(0);
            normal.Y.Should().Be(1);
            normal.Z.Should().Be(0);
        }

        [Fact]
        public void Angle_Straight()
        {
            var s1 = new Segment(Vertex.Zero - Vector.UnitY, Vertex.Zero);
            var s2 = new Segment(Vertex.Zero, Vertex.Zero + Vector.UnitY);

            s1.ConvexAngle(s2).Should().BeNull();
        }

        [Fact]
        public void Angle_ConCave()
        {
            // .---
            //    .
            //    |
            var s1 = new Segment(Vertex.Zero - Vector.UnitY, Vertex.Zero); // (0, -1, 0) => (0, 0, 0)
            var s2 = new Segment(Vertex.Zero, Vertex.Zero - Vector.UnitX); // (0, 0, 0) => (-1, 0, 0)

            s1.ConvexAngle(s2).Should().BeFalse();
        }

        [Fact]
        public void Angle_Obtuse()
        {
            var s1 = new Segment(Vertex.Zero - Vector.UnitY, Vertex.Zero);
            var s2 = new Segment(Vertex.Zero, Vertex.Zero + Vector.UnitX);

            s1.ConvexAngle(s2).Should().BeTrue();
        }

        [Fact]
        public void ToIntersection()
        {
            var s1 = new Segment(Vertex.Zero - Vector.UnitY, Vertex.Zero);
            var s2 = new Segment(Vertex.Zero, Vertex.Zero + Vector.UnitX);

            s1.ConvexAngle(s2).Should().BeTrue();
        }

        [Fact]
        public void Intersection_AreIntersecting()
        {
            // Given
            var one = Vertex.Zero - Vector.UnitX;
            var two = Vertex.Zero + Vector.UnitX + Vector.UnitY;
            var three = Vertex.Zero + Vector.UnitX;
            var four = Vertex.Zero - Vector.UnitX + Vector.UnitY;
            var s1 = new Segment(one, two);
            var s2 = new Segment(three, four);

            // When
            var intersection = s1.IntersectionOf(s2);

            // Then
            intersection.Y.Should().BeApproximately(0.5f, 0.0001f);
            intersection.Z.Should().BeApproximately(0.0f, 0.0001f);
            intersection.X.Should().BeApproximately(0.0f, 0.0001f);
        }

        [Fact]
        public void Intersection_BillIsWrongTest()
        {
            // Given
            var one = new Vertex(2, 1, 5);
            var two = new Vertex(1, 2, 5);
            var three = new Vertex(2, 1, 3);
            var four = new Vertex(2, 1, 2);
            var s1 = new Segment(one, two);
            var s2 = new Segment(three, four);

            // When
            var intersection = s1.IntersectionOf(s2);

            // Then
            intersection.Should().BeNull();
        }
    }
}
