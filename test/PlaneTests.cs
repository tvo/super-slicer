using System;
using cam.models;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace test
{
    public class PlaneTests
    {
        [Fact]
        public void Normal()
        {
            var one = Vertex.Zero;
            var two = Vertex.Zero + Vector.UnitX;
            var three = Vertex.Zero + Vector.UnitY;
            var plane = new Plane(one, two, three);
            var normal = plane.Normal();
            normal.X.Should().Be(0);
            normal.Y.Should().Be(0);
            normal.Z.Should().Be(1);
        }

        [Fact]
        public void IsOnNormal_True()
        {
            var one = Vertex.Zero + Vector.UnitZ;
            var two = Vertex.Zero + Vector.UnitX + Vector.UnitZ;
            var three = Vertex.Zero + Vector.UnitY + Vector.UnitZ;
            var plane = new Plane(one, two, three);
            var point = Vertex.Zero + 2 * Vector.UnitZ;

            var isOnNormal = plane.IsOnNormalSide(point);

            isOnNormal.Should().BeTrue();
        }

        [Fact]
        public void IsOnNormal_False()
        {
            var one = Vertex.Zero + Vector.UnitZ;
            var two = Vertex.Zero + Vector.UnitX + Vector.UnitZ;
            var three = Vertex.Zero + Vector.UnitY + Vector.UnitZ;
            var plane = new Plane(one, two, three);
            var point = Vertex.Zero;

            var isOnNormal = plane.IsOnNormalSide(point);

            isOnNormal.Should().BeFalse();
        }

        [Fact]
        public void IntersectsPoint()
        {
            var one = Vertex.Zero + Vector.UnitZ;
            var two = Vertex.Zero + Vector.UnitX + Vector.UnitZ;
            var three = Vertex.Zero + Vector.UnitY + Vector.UnitZ;
            var plane = new Plane(one, two, three);
            var segment = new Segment(Vertex.Zero, Vertex.Zero + 2 * Vector.UnitZ);

            var point = plane.IntersectionOf(segment);

            point.X.Should().Be(0);
            point.Y.Should().Be(0);
            point.Z.Should().Be(1);
        }

        [Fact]
        public void IntersectsTriangle()
        {
            var one = Vertex.Zero + Vector.UnitZ;
            var two = Vertex.Zero + Vector.UnitX + Vector.UnitZ;
            var three = Vertex.Zero + Vector.UnitY + Vector.UnitZ;
            var plane = new Plane(one, two, three);

            var triangle = new Triangle(Vertex.Zero, Vertex.Zero + 20 * Vector.UnitZ, Vertex.Zero + 2 * Vector.UnitY);

            var segment = plane.IntersectionOf(triangle).First();

            segment.One.X.Should().Be(0);
            segment.One.Y.Should().Be(0);
            segment.One.Z.Should().Be(1);
            
            segment.Two.X.Should().Be(0);
            segment.Two.Y.Should().BeGreaterThan(0.5f);
            segment.Two.Z.Should().Be(1);
        }
    }
}
