using System;
using cam.models;
using Xunit;
using FluentAssertions;
using System.Reflection;

namespace test
{
    public class ImmutableTests
    {
        /// This test verifies immutable structs
        [Fact]
        public void EnsureStructsAreImmutableTest()
        {
            Type t = typeof(Vector);
            var p = new Assembly[] { t.Assembly };
            new Immutability.Test(p).EnsureStructsAreImmutableTest();
        }

        /// This test verifies the immutability of classes.
        [Fact]
        public void EnsureImmutableTypeFieldsAreMarkedImmutableTest()
        {
            Type t = typeof(Vector);
            var p = new Assembly[] { t.Assembly };
            new Immutability.Test(p).EnsureImmutableTypeFieldsAreMarkedImmutableTest();
        }

    }
}
