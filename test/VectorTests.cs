using System;
using cam.models;
using Xunit;
using FluentAssertions;

namespace test
{
    public class VectorTests
    {
        [Fact]
        public void PropertiesSetCorrectly()
        {
            var vector = new Vector(1, 2, 3);
            vector.X.Should().Be(1);
            vector.Y.Should().Be(2);
            vector.Z.Should().Be(3);
        }

        [Fact]
        public void AddVector()
        {
            var vector = new Vector(1, 2, 3) + new Vector(1, 2, 3);
            vector.X.Should().Be(2);
            vector.Y.Should().Be(4);
            vector.Z.Should().Be(6);
        }

        [Fact]
        public void SubtractVector()
        {
            var vector = new Vector(1, 2, 3) - new Vector(1, 2, 3);
            vector.X.Should().Be(0);
            vector.Y.Should().Be(0);
            vector.Z.Should().Be(0);
        }

        [Fact]
        public void MultiplyScalar()
        {
            var vector = 2 * new Vector(1, 2, 3);
            vector.X.Should().Be(2);
            vector.Y.Should().Be(4);
            vector.Z.Should().Be(6);
        }

        [Fact]
        public void DivideScalar()
        {
            var vector = new Vector(2, 4, 6) / 2;
            vector.X.Should().Be(1);
            vector.Y.Should().Be(2);
            vector.Z.Should().Be(3);
        }

        [Fact]
        public void UnitLength()
        {
            var vector = new Vector(200, 0, 0).UnitLength();
            vector.X.Should().Be(1);
            vector.Y.Should().Be(0);
            vector.Z.Should().Be(0);
        }

        [Fact]
        public void Magnitude()
        {
            var mag = new Vector(200, 0, 0).Magnitude();
            mag.Should().Be(200);
        }

        [Fact]
        public void DotProduct()
        {
            var a = new Vector(6.42788f, 7.66044f, 0);
            var b = new Vector(5, 0, 0);
            var product = (double)a.DotProduct(b);
            product.Should().BeApproximately(32.1394, 0.01);
        }

        [Fact]
        public void CrossProduct()
        {
            var a = new Vector(2, 3, 4);
            var b = new Vector(5, 6, 7);
            var cross = a.CrossProduct(b);
            cross.X.Should().Be(-3);
            cross.Y.Should().Be(6);
            cross.Z.Should().Be(-3);
        }

        [Fact]
        public void CrossProduct_OfSameVector()
        {
            var a = new Vector(0, 1, 0);
            var cross = a.CrossProduct(a);
            cross.X.Should().Be(0);
            cross.Y.Should().Be(0);
            cross.Z.Should().Be(0);
        }

        [Fact]
        public void CrossProduct_OfSameVector_DifferentLength()
        {
            var a = new Vector(0, 1, 0);
            var cross = a.CrossProduct(a + Vector.UnitY);
            cross.X.Should().Be(0);
            cross.Y.Should().Be(0);
            cross.Z.Should().Be(0);
        }

        [Fact]
        public void CrossProduct_90ToTheRight()
        {
            var cross = Vector.UnitY.CrossProduct(Vector.UnitX);
            cross.X.Should().Be(0);
            cross.Y.Should().Be(0);
            cross.Z.Should().Be(-1);
        }

        [Fact]
        public void Projection_SameAxis()
        {
            var a = new Vector(5, 0, 0);
            var b = new Vector(1000, 0, 0);
            var projection = a.ProjectTo(b);
            projection.X.Should().Be(5);
            projection.Y.Should().Be(0);
            projection.Z.Should().Be(0);
        }

        [Fact]
        public void Projection_DifferentAxis()
        {
            var a = new Vector(5, 0, 0);
            var b = new Vector(0, 5550, 0);
            var projection = a.ProjectTo(b);
            projection.X.Should().Be(0);
            projection.Y.Should().Be(0);
            projection.Z.Should().Be(0);
        }

        [Fact]
        public void Projection_DiagnalOntoY()
        {
            var a = new Vector(5, 5, 5);
            var b = new Vector(0, 5550, 0);
            var projection = a.ProjectTo(b);
            projection.X.Should().Be(0);
            projection.Y.Should().Be(5);
            projection.Z.Should().Be(0);
        }
    }
}
