using cam.models;
using Xunit;
using FluentAssertions;

namespace test
{
    public class TriangleTests
    {
        [Fact]
        public void Normal()
        {
            var one = Vertex.Zero;
            var two = Vertex.Zero + Vector.UnitX;
            var three = Vertex.Zero + Vector.UnitY;
            var triangle = new Triangle(one, two, three);
            var normal = triangle.CalculateNormal();
            normal.X.Should().Be(0);
            normal.Y.Should().Be(0);
            normal.Z.Should().Be(1);
        }
    }
}
