using System;
using cam.models;
using Xunit;
using FluentAssertions;

namespace test
{
    public class RayTests
    {
        [Fact]
        public void PropertiesSetCorrectly()
        {
            var r = new Ray(Vertex.Zero + Vector.UnitX, Vector.UnitY);
            r.Origin.X.Should().Be(1);
            r.Direction.Y.Should().Be(1);
        }

        [Fact]
        public void Intersection_Origin()
        {
            // Given
            var rayY = new Ray(Vertex.Zero - Vector.UnitY, Vector.UnitY);
            var rayX = new Ray(Vertex.Zero - Vector.UnitX, Vector.UnitX);
            
            // When
            var intersection = rayX.Intersects(rayY);

            // Then
            intersection.X.Should().Be(0);
            intersection.Y.Should().Be(0);
            intersection.Z.Should().Be(0);
        }

        [Fact]
        public void Intersection_OneOne()
        {
            // Given
            var rayY = new Ray(Vertex.Zero + Vector.UnitX, Vector.UnitY);
            var rayX = new Ray(Vertex.Zero + Vector.UnitY, Vector.UnitX);
            
            // When
            var intersection = rayX.Intersects(rayY);

            // Then
            intersection.X.Should().Be(1);
            intersection.Y.Should().Be(1);
            intersection.Z.Should().Be(0);
        }
    }
}
