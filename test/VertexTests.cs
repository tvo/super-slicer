using System;
using cam.models;
using Xunit;
using FluentAssertions;

namespace test
{
    public class VertexTests
    {
        [Fact]
        public void PropertiesSetCorrectly()
        {
            var vertex = new Vertex(1, 2, 3);
            vertex.X.Should().Be(1);
            vertex.Y.Should().Be(2);
            vertex.Z.Should().Be(3);
        }

        [Fact]
        public void AddOneToX()
        {
            var vertex = new Vertex(1, 2, 3) + Vector.UnitX;
            vertex.X.Should().Be(2);
            vertex.Y.Should().Be(2);
            vertex.Z.Should().Be(3);
        }

        [Fact]
        public void AddOneToY()
        {
            var vertex = new Vertex(1, 2, 3) + Vector.UnitY;
            vertex.X.Should().Be(1);
            vertex.Y.Should().Be(3);
            vertex.Z.Should().Be(3);
        }

        [Fact]
        public void AddOneToZ()
        {
            var vertex = new Vertex(1, 2, 3) + Vector.UnitZ;
            vertex.X.Should().Be(1);
            vertex.Y.Should().Be(2);
            vertex.Z.Should().Be(4);
        }

        [Fact]
        public void SubtractVertex()
        {
            var vertex = new Vertex(1, 2, 3) - new Vertex(1, 2, 3);
            vertex.X.Should().Be(0);
            vertex.Y.Should().Be(0);
            vertex.Z.Should().Be(0);
        }

        [Fact]
        public void SubtractVector()
        {
            var vertex = new Vertex(1, 2, 3) - new Vector(1, 2, 3);
            vertex.X.Should().Be(0);
            vertex.Y.Should().Be(0);
            vertex.Z.Should().Be(0);
        }
    }
}
