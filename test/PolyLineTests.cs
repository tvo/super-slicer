using cam.models;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace test
{
	public class PolyLineTests
	{
		private static PolyLine Square(float width)
		{
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;

			var s1 = new Segment(one, two);
			var s2 = new Segment(two, three);
			var s3 = new Segment(three, four);
			var s4 = new Segment(four, one);

			var line = new PolyLine(new[] { s1, s2, s3, s4 });

			return line;
		}

		[Fact]
		public void FromPoints_TwoPoints_HasOneSegment()
		{
			// Given
			var one = new Vertex(1, 1, 1);
			var two = new Vertex(2, 2, 2);
			var points = new[] { one, two };

			// When
			var segments = PolyLine.FromPoints(points).Segments;

			// Then
			segments.Should().ContainSingle();
		}

		[Fact]
		public void FromPoints_ThreePoints_HasTwoSegments()
		{
			// Given
			var one = new Vertex(1, 1, 1);
			var two = new Vertex(2, 2, 2);
			var three = new Vertex(3, 3, 3);
			var points = new[] { one, two, three };

			// When
			var segments = PolyLine.FromPoints(points).Segments;

			// Then
			segments.Should().HaveCount(2);
		}



		[Fact]
		public void FromPoints_ThreePoints_DataIsCorrect()
		{
			// Given
			var one = new Vertex(1, 1, 1);
			var two = new Vertex(2, 2, 2);
			var three = new Vertex(3, 3, 3);
			var points = new[] { one, two, three };

			// When
			var segments = PolyLine.FromPoints(points).Segments;

			// Then
			segments.First().One.Should().Be(one);
			segments.First().Two.Should().Be(two);
			segments.Last().One.Should().Be(two);
			segments.Last().Two.Should().Be(three);
		}


		[Fact]
		public void Expand_Square()
		{
			// Given
			var line = Square(2);

			// When
			var expand = line.Expand(1);

			// Then
			expand.Segments.Should().HaveCount(8);
		}


		[Fact]
		public void Expand_Square_NoNan()
		{
			// Given
			var line = Square(2);

			// When
			var expand = line.Expand(1);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y }).Should().NotContain(float.NaN);
		}

		[Fact]
		public void Expand_Square_Subdivided_NoNaN()
		{
			// Given
			var width = 2;
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;

			var sides = new[] {
				new Segment(one, one + Vector.UnitY),
				new Segment(one + Vector.UnitY, two),
				new Segment(two, two + Vector.UnitX),
				new Segment(two + Vector.UnitX, three),
				new Segment(three, three - Vector.UnitY),
				new Segment(three - Vector.UnitY, four),
				new Segment(four, four - Vector.UnitX),
				new Segment(four - Vector.UnitX, one),
			};

			var line = new PolyLine(sides);

			// When
			var expand = line.Expand(0.01f);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y }).Should().NotContain(float.NaN);
		}

		[Fact]
		public void Expand_Square_Subdivided_NoNaN_WithNegativeZ()
		{
			// Given
			var width = 2;
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;

			one = one - Vector.UnitZ;
			two = two - Vector.UnitZ;
			three = three - Vector.UnitZ;
			four = four - Vector.UnitZ;

			var sides = new[] {
				new Segment(one, one + Vector.UnitY),
				new Segment(one + Vector.UnitY, two),
				new Segment(two, two + Vector.UnitX),
				new Segment(two + Vector.UnitX, three),
				new Segment(three, three - Vector.UnitY),
				new Segment(three - Vector.UnitY, four),
				new Segment(four, four - Vector.UnitX),
				new Segment(four - Vector.UnitX, one),
			};

			var line = new PolyLine(sides);

			// When
			var expand = line.Expand(0.01f);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y }).Should().NotContain(float.NaN);
		}

		[Fact]
		public void Expand_Square_Subdivided_NoNaN_SquareCornerAt_X0Y0()
		{
			// Given
			var width = 2;
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;

			one = one + Vector.UnitX + Vector.UnitY;
			two = two + Vector.UnitX + Vector.UnitY;
			three = three + Vector.UnitX + Vector.UnitY;
			four = four + Vector.UnitX + Vector.UnitY;

			var sides = new[] {
				new Segment(one, one + Vector.UnitY),
				new Segment(one + Vector.UnitY, two),
				new Segment(two, two + Vector.UnitX),
				new Segment(two + Vector.UnitX, three),
				new Segment(three, three - Vector.UnitY),
				new Segment(three - Vector.UnitY, four),
				new Segment(four, four - Vector.UnitX),
				new Segment(four - Vector.UnitX, one),
			};

			var line = new PolyLine(sides);

			// When
			var expand = line.Expand(0.01f);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y }).Should().NotContain(float.NaN);
		}

		[Fact]
		public void SquareWithProblems()
		{
			//Given
			var z = 0.0f;
			var one = new Segment(new Vertex(4, 0, z), new Vertex(0.5f, 0, z));
			var two = new Segment(new Vertex(0.5f, 0, z), new Vertex(0, 0, z));
			var three = new Segment(new Vertex(0, 0, z), new Vertex(0, 3.5f, z));
			var four = new Segment(new Vertex(0, 3.5f, z), new Vertex(0, 4, z));
			var five = new Segment(new Vertex(0, 4, z), new Vertex(3.5f, 4, z));
			var six = new Segment(new Vertex(3.5f, 4, z), new Vertex(4, 4, z));
			var seven = new Segment(new Vertex(4, 4, z), new Vertex(4, 0.5f, z));
			var eight = new Segment(new Vertex(4, 0.5f, z), new Vertex(4, 0, z));

			var line = new PolyLine(new[] { one, two, three, four, five, six, seven, eight });

			// When
			var expand = line.Expand(0.01f);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y, v.Z }).Should().NotContain(float.NaN);
		}

		[Fact]
		public void Expand_Square_MaxScalarNotGreaterThan2()
		{
			// Given
			var line = Square(2);

			// When
			var expand = line.Expand(1);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y });
		}


		[Fact]
		public void Expand_Square_Hole()
		{
			// Given
			var width = 20;
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;
			var s1 = new Segment(one, four);
			var s2 = new Segment(four, three);
			var s3 = new Segment(three, two);
			var s4 = new Segment(two, one);
			var line = new PolyLine(new[] { s1, s2, s3, s4 });

			// When
			var expand = line.Expand(1);

			// Then
			expand.Segments.Should().HaveCount(4);
		}

		[Fact]
		public void Expand_TriangleAt_X0Y0()
		{
			// Given
			var one = Vertex.Zero;
			var two = one + Vector.UnitY * 2;
			var three = one + Vector.UnitX * 2;

			var sides = new[] {
				new Segment(one, two),
				new Segment(two, three),
				new Segment(three, one)
			};

			var line = new PolyLine(sides);

			// When
			var expand = line.Expand(0.01f);

			// Then
			expand.ToVertices().SelectMany(v => new[] { v.X, v.Y }).Should().NotContain(float.NaN);
		}

		[Fact]
		public void ConvexCornerTest()
		{
			// Given
			var one = Vertex.Zero - Vector.UnitX * 2;
			var two = Vertex.Zero - Vector.UnitX;
			var three = Vertex.Zero - Vector.UnitY;
			var four = Vertex.Zero - Vector.UnitY * 2;
			var corner = Vertex.Zero - Vector.UnitX - Vector.UnitY;
			var s1 = new Segment(one, two);
			var s2 = new Segment(three, four);

			// When
			var (left, cutoff, right) = PolyLine.GetContuationOfLinesAroundCorner(s1, s2, corner, 1);

			// Then
			var cutoffDistance = -0.5857f;
			left.One.Should().Be(one);
			left.Two.X.Should().BeApproximately(cutoffDistance, 0.001f);
			right.Two.Should().Be(four);
			right.One.Y.Should().BeApproximately(cutoffDistance, 0.001f);
			cutoff.One.Should().Be(left.Two);
			cutoff.Two.Should().Be(right.One);
		}


		[Fact]
		public void Square_Clockwise()
		{
			// Given
			var line = Square(2);

			// When
			var cw = line.IsClockwise();

			// Then
			cw.Should().BeTrue();
		}

		[Fact]
		public void SquareHole_CounterClockwise()
		{
			// Given
			var width = 20;
			var one = Vertex.Zero - width / 2 * (Vector.UnitX + Vector.UnitY);
			var two = one + width * Vector.UnitY;
			var three = two + width * Vector.UnitX;
			var four = three - width * Vector.UnitY;
			var s1 = new Segment(one, four);
			var s2 = new Segment(four, three);
			var s3 = new Segment(three, two);
			var s4 = new Segment(two, one);
			var line = new PolyLine(new[] { s1, s2, s3, s4 });

			// When
			var ccw = line.IsClockwise();

			// Then
			ccw.Should().BeFalse();
		}

		[Fact]
		public void Union_ShouldResultInClockwisePolygon()
		{
			// Given
			var t1_one = Vertex.Zero + 0.5f * Vector.UnitX;
			var t1_two = Vertex.Zero - Vector.UnitX - Vector.UnitY;
			var t1_three = Vertex.Zero - Vector.UnitX + Vector.UnitY;
			var t2_one = Vertex.Zero - 0.5f * Vector.UnitX;
			var t2_two = Vertex.Zero + Vector.UnitX + Vector.UnitY;
			var t2_three = Vertex.Zero + Vector.UnitX - Vector.UnitY;
			var t1 = new Triangle(t1_two, t1_three, t1_one);
			var t2 = new Triangle(t2_one, t2_two, t2_three);
			var line1 = new PolyLine(t1.GetAllSides());
			var line2 = new PolyLine(t2.GetAllSides());

			// When
			var union = line1.Union(line2);

			// Then
			union.IsClockwise().Should().BeTrue();
		}

		[Fact]
		public void Union_TwoTranglesFacingEachOther()
		{
			// Given
			var t1_one = Vertex.Zero + 0.5f * Vector.UnitX;
			var t1_two = Vertex.Zero - Vector.UnitX - Vector.UnitY;
			var t1_three = Vertex.Zero - Vector.UnitX + Vector.UnitY;
			var t2_one = Vertex.Zero - 0.5f * Vector.UnitX;
			var t2_two = Vertex.Zero + Vector.UnitX + Vector.UnitY;
			var t2_three = Vertex.Zero + Vector.UnitX - Vector.UnitY;
			var t1 = new Triangle(t1_two, t1_three, t1_one);
			var t2 = new Triangle(t2_one, t2_two, t2_three);
			var line1 = new PolyLine(t1.GetAllSides());
			var line2 = new PolyLine(t2.GetAllSides());

			// When
			var union = line1.Union(line2);
			var points = union.ToVertices();

			// Then
			union.Segments.Should().HaveCount(6);
			points.Should().NotContainNulls();
			points.Should().ContainEquivalentOf(new Vertex(-1, -1, 0));
			points.Should().ContainEquivalentOf(new Vertex(-1, 1, 0));
			points.Should().ContainEquivalentOf(new Vertex(1, -1, 0));
			points.Should().ContainEquivalentOf(new Vertex(-1, 1, 0));
			points.Should().ContainEquivalentOf(new Vertex(0, 1.0f / 3, 0));
			points.Should().ContainEquivalentOf(new Vertex(0, -1.0f / 3, 0));
			points.Should().HaveCount(7);
		}

		[Fact]
		public void Union_NestedSquares_CombinesIntoOne()
		{
			// Given
			var big = PolyLine.Square(20);
			var small = PolyLine.Square(1);

			// When
			var union = big.Union(small);

			// Then
			union.Segments.Should().HaveCount(4);
		}
	}
}
