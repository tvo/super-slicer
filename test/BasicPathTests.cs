using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;
using cam.models;
using cam.PathGenerators;

namespace test
{
	public class BasicPathTests
	{
		[Fact]
		public void TwoSquares_RemovesBridges()
		{
			// Given
			var bigTopCube = Model.Cube().Translate(Vector.UnitZ).Scale(new Vector(2, 2, 2));
			var littleBottomCube = Model.Cube().Translate(Vector.UnitZ * -1);
			var cubeWithOverhangBridge = bigTopCube.Union(littleBottomCube);

			// When
			var cutPath = new Basic(cubeWithOverhangBridge).Process(0.52f, 0.1f);

			// Then
			var x = cutPath.Last().X;
			var y = cutPath.Last().Y;
			(x > 0 && y > 0).Should().BeFalse(); // if x and y are positive it is in quadrent 1. That means that it is under the big square.
		}

		[Fact]
		public void TwoSquares_RemovesLedges()
		{
		}

		[Fact]
		public void Home_CentersModel()
		{
			// Given
			var model = Model.Cube();

			// When
			var points = model.HomeModel().GetVertices();

			// Then
			points.Max(x => x.Z).Should().Be(0);
			points.Min(x => x.X).Should().Be(0);
			points.Min(x => x.Y).Should().Be(0);
		}

		[Fact]
		public void Slice_HasTwoSlices()
		{
			// Given
			var model = Model.Cube();

			// When
			var sliced = new Basic(model).SliceModel(1);

			// Then
			sliced.Should().HaveCount(1);
		}

		[Fact]
		public void Slice_DoesNotSliceTopLayer()
		{
			// Given
			var model = Model.Cube();

			// When
			var sliced = new Basic(model).SliceModel(1);

			// Then
			sliced.SelectMany(x => x).SelectMany(x => x.ToVertices()).Max(x => x.Z).Should().NotBe(1);
		}

		[Fact]
		public void Slice_ConnectsSlices()
		{
			// Given
			var model = Model.Cube();

			// When
			var path = PolyLine.FromPoints(new Basic(model).Process(0.25f, 1));

			// Then
			var first = path.Segments.First(x => x.One.Z == -0.25 && x.Two.Z == -0.5);
			first.Should().NotBeNull();
		}

		[Fact]
		public void Slice_HasOneStraightEntryPlungeCut()
		{
			// Given
			var model = Model.Cube();

			// When
			var path = PolyLine.FromPoints(new Basic(model).Process(1, 1));

			// Then
			var first = path.Segments.First();
			first.One.Z.Should().BeApproximately(0, 0.0001f);
			first.Two.Z.Should().BeApproximately(-1, 0.0001f);
		}

		[Fact]
		public void Slice_ExpandPolyLine_No_NaN_Returned()
		{
			// Given
			var model = Model.Cube();
			model = model.Translate(Vector.UnitZ).Scale(new Vector(2, 2, 2));
			var basic = new Basic(model);

			// When
			var cutPath = basic.Process(0.5f, 0.01f);

			// Then
			cutPath.SelectMany(v => new [] { v.X, v.Y, v.Z }).Should().NotContain(float.NaN);
		}
	}
}
